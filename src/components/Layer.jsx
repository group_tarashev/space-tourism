import React from 'react'

const Layer = ({numb, title, children}) => {
  return (
    
    <div className="dest-outer">
      <h1 className="title">
        <span>0{numb}</span> {title}      </h1>
        {children}
    </div>
  )
}

export default Layer