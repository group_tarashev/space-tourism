import React from "react";

const Content = ({ sub, title, p }) => {
  return (
    <div className="lay-content">
      {sub.length !== 0 && <h2 className="role">{sub}</h2>}
      <h1 className="name">{title}</h1>
      <p className="lay-desc">{p}</p>
    </div>
  );
};

export default Content;
