import React, { useEffect, useState } from "react";
import "../styles/nav.css";
const nav = ["home", "destionation", "crew", "technology"];
const Nav = ({open, setOpen}) => {
    
    const behavior = {behavior: 'smooth'}
    const [width, setWidth] = useState('home')
    const handleScroll =(i) =>{
        setOpen(false)
        const dest = document.querySelector('.destination');
        const home = document.querySelector('.home')
        const crew = document.querySelector('.crew');
        const tech = document.querySelector('.technology')
        setWidth(nav[i])
        if(i === 0){
            home && home.scrollIntoView(behavior);
        }
        if(i ===1){
            dest && dest.scrollIntoView(behavior)
        }
        if(i == 2){
            crew && crew.scrollIntoView(behavior)
        }
        if(i === 3){
            tech && tech.scrollIntoView(behavior)
        }
    }
  return (
    <div className={open ? "nav nav-open": "nav"}>
      <ul className="nav-list">
        <button onClick={() => setOpen(false)} className="close-menu">X</button>
        {nav.map((item, i) => (
          <li style={{borderWidth:width == item &&'3px'}} onClick={() => handleScroll(i)} key={i} className={width === item? "nav-li active" : "nav-li"}>
            <span>0{i}</span>

            {item}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Nav;
