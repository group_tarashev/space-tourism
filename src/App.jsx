import React, { useState } from "react";
import "./styles/App.css";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Home from "./pages/Home";
import Destination from "./pages/Destination";
import Crew from "./pages/Crew";
import Technology from "./pages/Technology";
import Nav from "./components/Nav";

const App = () => {
  const [open, setOpen] = useState(false)
  return (
      <div className="App">
        <Nav setOpen={setOpen} open={open}/>
        <img src="./assets/shared/logo.svg" className="logo" alt="logo" />
        <button onClick={() => setOpen(true)} className="menu"><span className="menu-line"></span></button>
        <div className="hr-line"/>
        <div className="content-app">
          <Home/>
          <Destination/>
          <Crew/>
          <Technology/>
        </div>
      </div>
  );
};

export default App;
