import axios from "axios";
import { createContext, useEffect, useState } from "react";

export const ContextApi = createContext();

const ContextProvider = ({ children }) => {
  const [data, setData] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const request = async () => {
        try {
            const res = await axios.get("./assets/data.json");
            setData(res.data);
            setIsLoading(false);
            
        } catch (error) {
            console.log(error)
        }
    };

    request();
  }, []);

  return (
    <ContextApi.Provider value={{ data, isLoading }}>
      {children}
    </ContextApi.Provider>
  );
};

export default ContextProvider;
