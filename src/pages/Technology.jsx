import React, { useContext, useState } from 'react'
import '../styles/technology.css'
import Layer from '../components/Layer'
import { ContextApi } from '../context/ContextApi'
import Content from '../components/Content'
const Technology = () => {
  const {data, isLoading} = useContext(ContextApi)
  const [index,setIndex] = useState(0);
  if(isLoading){
    return <h2>Loading ...</h2>
  }
  const handleIndex =(i) =>{
    setIndex(i)
  }
  const content = data.technology.map((item, i) =>{
    if(data.technology[index].name == item.name){
      return (
      <div key={i} className="tech-inner">
        <div className="tech-left">
          <div className="tabs">{
            data.technology.map((tab, j) =>(
              <span onClick={() => handleIndex(j)} className={ data.technology[j].name == item.name ?'tab tab-active' : 'tab'}>{j + 1}</span>
            ))
          }</div>
          <Content title={item.name} p={item.description} sub={''}/>
        </div>
        <div className="tech-right"><img className='images' src={item.images.portrait} alt="" /></div>
      </div>)
    }
  })
  return (
    <div className='technology'>
      <Layer numb={3} title={'space launch 101'}>{content}</Layer>
    </div>
  )
}

export default Technology