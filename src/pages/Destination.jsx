import React, { useContext, useEffect, useState } from "react";
import "../styles/destination.css";
import { ContextApi } from "../context/ContextApi";
import Layer from "../components/Layer";
const Destination = () => {
  const { data, isLoading } = useContext(ContextApi);
  const [dest, setDest] = useState([])
  const [index,setIndex]= useState(0);
  const [border,setBorder] = useState('moon')
  if (isLoading) {
    return <h1>Loading ...</h1>;
  }
  const handleContent = (i) =>{
    setBorder(data.destinations[i].name.toLowerCase())
    setIndex(i)
  }  
  console.log(data)
  const content = data?.destinations.map((item, i) => {
    if (data?.destinations[index].name == item.name) {
      return (
        <div key={i} className="dest-inner">
          <div className="dest-left">
            <img src={item.images.png} alt="" className="image" />
          </div>
          <div className="dest-right">
            <div className="tabs">
              <ul>
                {data.destinations.map((tab, j) => (
                  <li onClick={() => handleContent(j)} className={border == tab.name.toLowerCase() && " active"} key={j}>{tab.name}</li>
                ))}
              </ul>
            </div>
            <h1 className="dest-name">{item.name}</h1>
            <p className="dest-desc">{item.description}</p>
            <div className="line"/>
            <div className="distance">
              <div className="dist-to">
                <span>avg. deistance</span>
                <h1>{item.distance}</h1>
              </div>
              <div className="travel">
                <span>est. travel time</span>
                <h1>{item.travel}</h1>
              </div>
            </div>
          </div>
        </div>
      );
    }
  });

  return (
    <div className="destination">
      <Layer numb={1} title={'pick your destiantion'}>{content}</Layer>
    </div>
  );
};

export default Destination;
