import React from "react";
import "../styles/home.css";
const Home = () => {
  return <div className="home">
    <div className="home-content">
      <div className="home-left">
        <h1 className="so">so, you want to travel to</h1>
        <h1 className="space">Space</h1>
        <p className="space-cont">Let`s face it: If you want to go to space, you might as well genuinely go to outer soace and not hover kind of on the edge it. Well sit back, and relax becouse we`ll give you a truly out of this experience!</p>
      </div>
      <div className="home-right">
        <div className="explore"><h1>Explore</h1></div>     
      </div>
    </div>
  </div>;
};

export default Home;
