import React, { useCallback, useContext, useEffect, useRef, useState } from 'react'
import '../styles/crew.css'
import Layer from '../components/Layer'
import { ContextApi } from '../context/ContextApi'
import Content from '../components/Content'
const Crew = () => {
  const {data, isLoading} = useContext(ContextApi)
  const [index, setIndex] = useState(0)
  const interval = useRef(5000);
  useEffect(()=>{
    const time = setInterval(()=>{
      setIndex(prev => (prev + 1) % 4)
    },interval.current)
    return () => { clearInterval(time)}
  },[])
  
  if(isLoading){
    return <h2>Loading ... </h2>
  }
  const handleCrew =(i) =>{
    setIndex(i)
    interval.current = 5000

  }
  const content = data.crew.map((item, i) =>{
    if(data.crew[index].name == item.name){
      return (
        <div className='crew-inner' key={i}>
          <div className="crew-left">
            <Content sub={item.role} title={item.name} p={item.bio} />
            <div className="dot-tabs">{
              data.crew.map((tab, j) =>(
                <span key={j} onClick={() => handleCrew(j)} className={ data.crew[j].name == item.name ? 'dot-tab dot-tab-active' : 'dot-tab'}></span>
              ))
            }</div>
          </div>
          <div className="crew-right"><img className='image' src={item.images.png} alt="" /></div>  
        </div>

      )
    }
  })
  
  return (
    <div className='crew'>
      <Layer numb={2} title={"meet your crew"}>{content}</Layer>
    </div>
  )
}

export default Crew